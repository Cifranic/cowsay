FROM centos:7
MAINTAINER Nicholas M. Cifranic "nicifranic@gmail.com"

RUN yum -y install wget \
    && wget http://springdale.math.ias.edu/data/puias/7/x86_64/os/Addons/Packages/fortune-mod-1.99.1-17.sdl7.x86_64.rpm \
    && wget http://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/c/cowsay-3.04-4.el7.noarch.rpm \
    && yum -y install *.rpm

CMD /usr/bin/fortune -a | /usr/bin/cowsay